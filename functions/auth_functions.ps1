# Function that will read in a file that contains a secure string hash and decrypt it to plain text.
function decryptFile {
	param ([string]$filename, [string]$key_file) 
	$key = gc $key_file
	$data = gc $filename | ConvertTo-SecureString -key $key
	$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($data)
	$pstr = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
	
	return $pstr
}